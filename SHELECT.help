<!--
  SHELECT is a simple shell program to use 'cut', 'awk', 'join' and
  'sort' in a easy, concise and SQL like way.

	 Copyright (C) 2012,2018 Dominique Corfa dominique.corfa@free.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>SHELECT HELP</title>
</head>
<body>
<pre>
syntax: SELECT [-s|--separator sep]
    [column1 [column2 ...] | '*']
    FROM table1 [JOIN table2 ON column3=column4 ...]
    WHERE where_expression
    [ORDER [BY] column5 [column6 ...] [ASC|DESC]]
	[GROUP [BY] column7 [column8 ...]]


Simple SQL like agregator for the join, cut and sort unix
commands operating on text files. Clauses may be written
in uppercase or downcase.

The tables are simple text files, one line per record,
each column separated by the separator ','.  Lines
begining with '#' are comments. The first line must be a
comment containing the column names separated by... the
separator.

Columni may be of the form 'table.column' or only 'column'
if the column name is unique among all the columns.

options:

-s or --separator
    use 'sep' as columns separator (default to comma ',').

SQL Clause:

SELECT

    retains only the desired columns. Column name may be aliases with
    the "column1 AS c1" syntax, givin columns1 the alias c1. The alias
    is used only for the result table. Future development will use
    them in all other clauses.

	'*' may be used for all columns. Take care of protecting the '*'
    against shell expansion. '*' must be used alone, without any other
    column names.

    The following aggretion function may be used in conjonction with a
    GROUP clause: "count(column)" (col may be omitted), "sum(column)",
    "max(column)", "min(column)" and they must be protected from shell
    interpretation.

FROM

    gives the desired tables. The special table name '-' is the
    standard input, enabling a kind of subquery.

JOIN

    gives the columns the tables are joined on. JOIN
    requires a ON argument

ON

    the only accepted syntax is 'columni=columnj'

WHERE

	where_expression is an awk expression for selecting line, in which
	the positionnal parameters are replaced bythe column names. For
	instance, to select line with column matching a regexp, you can
	write 'colmumnj~/^John/||colmumnj~/^Mary/'. Operator may combine
	columns of different table but they do not create JOIN relation.

ORDER BY

    sorts the resulting set on the given colum. ASC and DESC indicate
    ascending (resp. descending) order.

GROUP BY

	keep only one occurence (the first one) of each set of row having
	the same column value.  May be used or not with an aggregation
	function.

SAMPLE

 printf "# a,b,c\nx,3,a\nz,1,a\ny,8,a\nw,10,b" | SHELECT c "sum(b)" FROM - WHERE 'a<"z"' GROUP BY c
</pre>
</body>
</html>
